# CKEditor Better Collapse

This module makes only the 2nd button row collapsible.

To use it:

* enable the module
* go to *admin/config/content/formats/manage/basic_html* (or any other text format)
* Under *CKEditor plugin settings*, go to the *CKEditor Better Collapse* vertical tab,
  check the *enabled* widget, and save.

On edit pages with that text format, you will now find the 2nd button row collapsed
and a little triangle to uncollapse it. 

Note that you will **not** find a button to drag, as this module does not need one.
