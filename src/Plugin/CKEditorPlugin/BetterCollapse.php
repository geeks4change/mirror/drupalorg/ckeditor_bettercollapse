<?php

namespace Drupal\ckeditor_bettercollapse\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "customconfig" plugin.
 *
 * @CKEditorPlugin(
 *   id = "ckeditor_bettercollapse",
 *   label = @Translation("CKEditor Better Collapse")
 * )
 */
class BetterCollapse extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface, CKEditorPluginContextualInterface {

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_bettercollapse') . '/ckeditor-bettercollapse.js';
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
    return $editor->getSettings()['plugins']['ckeditor_bettercollapse']['enabled'] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = [];
    $settings = $editor->getSettings();
    $enabled = $settings['plugins']['ckeditor_bettercollapse']['enabled'] ?? FALSE;
    if ($enabled) {
      $config['toolbarCanCollapse'] = TRUE;
      $config['toolbarStartupExpanded'] = FALSE;
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {

    $settings = $editor->getSettings();
    $enabled = $settings['plugins']['ckeditor_bettercollapse']['enabled'] ?? FALSE;

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('CKEditor Better Collapse enabled'),
      '#default_value' => $enabled,
      '#description' => $this->t('If enabled, the toolbar gets a collapse button which collapses only the 2nd row.'),
    ];

    return $form;
  }

}
